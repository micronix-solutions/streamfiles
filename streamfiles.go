/*
Copyright 2020 Micronix Solutions, LLC (https://micronix.solutions/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package streamfiles

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/fsnotify/fsnotify"
	"gitlab.com/micronix-solutions/logging"
)

type FileStreamerConfig struct {
	WatchPath string
}

type FileStreamChunk struct {
	FileName string
	Data     []byte
}

type FileStreamer struct {
	Config FileStreamerConfig
	Logger logging.Logger

	stopSignal          chan struct{}
	knownFilesPositions map[string]int64
	knownPositionsMtx   sync.Mutex
	wg                  sync.WaitGroup
}

func (me *FileStreamer) Start() (<-chan FileStreamChunk, error) {
	streamChan := make(chan FileStreamChunk)
	me.stopSignal = make(chan struct{})
	if me.knownFilesPositions == nil {
		me.knownPositionsMtx.Lock()
		me.knownFilesPositions = make(map[string]int64)

		me.Logger.Debug("Read known files list")
		fdata, err := ioutil.ReadFile("knownfiles.g")

		if err != nil {
			me.Logger.Debug(err.Error())
		} else {
			kfstring := string(fdata)

			for _, line := range strings.Split(kfstring, "\n") {
				lineData := strings.Split(line, "|")
				fname := lineData[0]
				fposition, err := strconv.ParseInt(lineData[1], 10, 64)

				if err != nil {
					me.Logger.Err("Parse Error: " + err.Error())
					return nil, err
				}
				me.knownFilesPositions[fname] = fposition

			}
		}
		me.knownPositionsMtx.Unlock()
	}

	me.Logger.Info("Watch for changes")
	me.wg.Add(1)
	me.readFilesStream(streamChan)
	me.loopSaveFilePositions()
	return streamChan, nil
}

func (me *FileStreamer) Stop() {
	close(me.stopSignal)
	me.wg.Wait()

	me.saveFilePositions()
}

func (me *FileStreamer) saveFilePositions() {
	me.Logger.Debug("Save known files list")
	var buf bytes.Buffer

	me.knownPositionsMtx.Lock()

	for f, o := range me.knownFilesPositions {
		buf.WriteString(fmt.Sprintf("%s|%d\n", f, o))
	}

	me.knownPositionsMtx.Unlock()

	if buf.Len() > 0 {
		if err := ioutil.WriteFile("knownfiles.g", bytes.TrimRight(buf.Bytes(), "\n"), 0664); err != nil {
			me.Logger.Err("Error saving known files: " + err.Error())
		}
	} else {
		me.Logger.Info("Nothing to save")
	}
}

func (me *FileStreamer) readFilesStream(streamchan chan<- FileStreamChunk) {

	w, e := fsnotify.NewWatcher()
	if e != nil {
		me.Logger.Err(e.Error())
		return
	}
	go func() {
		me.Logger.Debug("Enter readFilesStream func")
		defer me.wg.Done()
		defer me.Logger.Debug("Exit readFilesStream func")
		for {
			select {
			case evt := <-w.Events:
				me.Logger.Debug("Event for " + evt.Name)
				switch evt.Op {
				case fsnotify.Write:
					file := evt.Name
					me.knownPositionsMtx.Lock()
					offset := me.knownFilesPositions[file]
					me.knownPositionsMtx.Unlock()
					handle, err := os.Open(file)
					if err != nil {
						me.Logger.Err("Open error: " + err.Error())
						continue
					}
					if _, err := handle.Seek(offset, 0); err != nil {
						me.Logger.Err("Seek error: " + err.Error())
						handle.Close()
						continue
					}
					data, err := ioutil.ReadAll(handle)
					handle.Close()
					if err != nil {
						me.Logger.Err("Open error: " + err.Error())
						continue
					}
					if len(data) > 0 {
						me.knownPositionsMtx.Lock()
						me.knownFilesPositions[file] = offset + int64(len(data))
						me.knownPositionsMtx.Unlock()
						streamchan <- FileStreamChunk{evt.Name, data}
					}

				case fsnotify.Remove:
					me.knownPositionsMtx.Lock()
					delete(me.knownFilesPositions, evt.Name)
					me.knownPositionsMtx.Unlock()
				}

			case err := <-w.Errors:
				me.Logger.Err(err.Error())

			case <-me.stopSignal:
				w.Remove(me.Config.WatchPath)
				close(streamchan)
				return
			}
		}
	}()

	w.Add(me.Config.WatchPath)

}

func (me *FileStreamer) loopSaveFilePositions() {
	t := time.NewTicker(10 * time.Second)
	me.wg.Add(1)
	go func() {
		defer me.wg.Done()
		for {
			select {
			case <-me.stopSignal:
				return
			case <-t.C:
				me.saveFilePositions()
			}
		}

	}()
}
