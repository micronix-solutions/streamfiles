module gitlab.com/micronix-solutions/streamfiles

go 1.12

require (
	github.com/fsnotify/fsnotify v1.4.7
	gitlab.com/micronix-solutions/logging v0.1.0
)
